#include <curl/curl.h>
#include <string.h>
#include <stdlib.h>
#define MULT 10 * 4096

struct content_data {
	int size;
	int length;
	char *data;
};

size_t write_callback(char *ptr, size_t size, size_t nmemb, void *userdata)
{
	struct content_data *cont = userdata;
	int realsize = size * nmemb;
	if(realsize > cont->size - cont->length) {
		char *tmp = realloc(cont->data, cont->size + realsize + 1);
		if(tmp == NULL)
			return 0;  /* out of memory! */
		cont->data = tmp;
		cont->size += realsize;
	}
	memcpy(&(cont->data[cont->length]), ptr, realsize);
	cont->length += realsize;
	cont->data[cont->size] = 0;

	return realsize;
}

/* Perfom an curl request to the url and fill @data with the response
 * The fucntion will resize @data if needed and update @ds
 * @url - the url to acces
 * @data - pointer to memoryt to write the response from the request
 * @ds - data sezi
 * @return - the HTTP response code from the request */
long request(const char* url, char** data, size_t *ds)
{

	CURL *curl;
	CURLcode res;
	struct content_data content;
	long code;

	curl = curl_easy_init();
	if(curl) {
		curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
		curl_easy_setopt(curl, CURLOPT_URL, url);

		if(! *data) {
			*data = malloc(4098);
		}
		content.data = *data;
		content.size = *ds;
		content.length = 0;
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &content);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_callback);

		/* Perform the request, res will get the return code */ 
		res = curl_easy_perform(curl);
		/* Check for errors */ 
		if(res != CURLE_OK)
			fprintf(stderr, "curl_easy_perform() failed: %s\n",
		curl_easy_strerror(res));
		res = curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &code);

		/* always cleanup */ 
		curl_easy_cleanup(curl);

		// update the size for the caller
		*ds = content.size;
		*data = content.data;
	} else {
		perror("curl init");
	}

	return code;
}

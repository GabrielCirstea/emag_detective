/* db.h
 * work with sqlite3 database */
#ifndef H_DB
#define HDB

typedef void sqlite3;

int init_db();
int add_in_db(const char* name, const char *url, const char *id, float price);
float last_price(sqlite3 *db, const char *id);
int db_count_id(sqlite3 *db, const char *id, const char *table);
int db_close();

#endif

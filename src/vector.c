/* vector.c
 * some shit vector to help with array stuff*/
#include "vector.h"
#include <stdlib.h>
#include <stdio.h>

#define err(x) do { fprintf(stderr, "File: %s, Line: %d\n", __FILE__, __LINE__);\
					perror(x);\
				}while(0);
#define INC_SPACE 10

/* init the vector
 * @l - the maximum length
 * @s - size of an element
 * @fn - the function to call to free the elemetns*/
vec vec_init(int l, int s, void (*fn)(void* e))
{
	vec v = malloc(sizeof(*v));
	if(!v) {
		err("vec_init: malloc");
		return NULL;
	}
	v->length = 0;
	v->size = l;
	v->e_size = sizeof(char*);
	v->v = malloc(l * sizeof(*v->v));
	if(!v->v) {
		err("vec_init: malloc v.v");
		free(v);
		return NULL;
	}
	if (fn == NULL)
		fn = free;
	v->free_fn = fn;
	return v;
}

int vec_free(vec v)
{
	for(int i=0; i<v->length; ++i) {
		// v->free_fn(v->v[i]);
		free(v->v[i]);
	}
	free(v);
	return 0;
}

/* push elements in vector
 * @v - the vector/array
 * @e - the element, the mem of elem will not be copied */
int vec_push(vec v, void* e)
{
	// make some space
	if(v->length > v->size){
		v->size += INC_SPACE;
		void **p = realloc(v->v, v->size * v->e_size);
		if(!p) {
			err("vec_push: realloc");
			return -1;
		}
		v->v = p;
	}
	int i = v->length;
	v->v[i] = e;
	v->length++;
	return 0;
}

void test_vec()
{
	vec v = vec_init(1,sizeof(int), free);
	int *a = malloc(sizeof(a));
	int *b = malloc(sizeof(b));
	int *c = malloc(sizeof(c));
	*a = 2;
	*b = 3;
	*c = 5;
	vec_push(v,a);
	printf("pushat %d\n", v->length);
	vec_push(v,b);
	printf("pushat %d\n", v->length);
	int *ints = v->v[1];
	printf("v[1]: %d\n", *ints);

	vec_free(v);
}

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "vector.h"

#define err(x) do { fprintf(stderr, "File: %s, Line: %d\n", __FILE__, __LINE__);\
					perror(x);\
				}while(0);
/* get the first html tag it finds
 * @page - the string that has html tags
 * @returns - a char* with the tag, no other attributes
 *				the memory should be freed*/
char *get_tag(const char *page)
{
	char *start = strchr(page, '<');
	if(!start) {
		fprintf(stderr, "get_tag: Not '<' in page\n");
		return NULL;
	}
	char *end = strchr(start, '>');
	if(!end) {
		fprintf(stderr, "get_tag: No '>' in page");
		return NULL;
	}
	char *space = strchr(start, ' ');
	if(space < end)
		end = space;
	char *tag = malloc(end-start);
	if(!tag) {
		err("get_tag malloc");
		return NULL;
	}
	strncpy(tag, start + 1, end-start-1);
	tag[end-start-1] = '\0';
	return tag;
}

/* strip the first html tag
 * @page - the html page
 * @returns - a pointer to the InnerHTML of the tag, the memory need to be freed*/
char *strip_tag(const char *page)
{
	char *tag = get_tag(page);
	if(!tag)
		return NULL;
	char opened[256] = {0}, closed[256] = {0};
	sprintf(opened,"<%s", tag);
	sprintf(closed,"</%s>", tag);
	free(tag);
	int op_l = strlen(opened),
		cl_l = strlen(closed);
	char *begin = strchr(page, '<');
	int stop_i = -1;
	begin = strchr(begin, '>') + 1;
	int count = 1;
	for(int i=0; begin[i]; ++i) {
		if(begin[i] == '<') {
			// is open?
			if(strncmp(begin+i, opened, op_l) == 0) {
				count++;
			}
			if(strncmp(begin+i, closed, cl_l) == 0) {
				count--;
			}
			if(count == 0) {
				stop_i = i-1;
				break;
			}
		}
	}
	if(stop_i < 1) {
		fprintf(stderr, "strip_tag: Bad end\n");
		return NULL;
	}
	char *content = malloc(stop_i + 1);
	if(!content) {
		err("malloc");
		return NULL;
	}
	strncpy(content, begin, stop_i);
	return content;
}

vec izolate_class(const char *page, const char *tag, const char *class)
{
	char format[256];
	sprintf(format,"<%s class=\"%s\">", tag, class);
	vec ps = vec_init(10, sizeof(char*), free);
	if(!ps) {
		err("izolate_class: vec_init");
		return NULL;
	}
	while((page = strstr(page, format)) != NULL) {
		char *res = strip_tag(page);
		if(!res) {
			fprintf(stderr, "izolate_class: strip return NULL\n");
			return ps;
		}
		vec_push(ps, res);
		page++;
	}
	return ps;
}

/* get the url f product from html
 * @product - html that will be parsed
 * @returns - an allocated string with the url, free the memory after use */
char *get_url(const char *product)
{
	if(!product)
		return NULL;
	char piv[] = "card-v2-info\"><a href=\"";
	char *start = strstr(product, piv);
	if(!start) {
		fprintf(stderr, "get_url: No start\n");
		return NULL;
	}
	start += strlen(piv);
	char *end = strchr(start, '"');
	if(!end) {
		fprintf(stderr, "get_url: N end\n");
		return NULL;
	}
	char *url = malloc(end-start + 1);
	strncpy(url, start, end-start);
	return url;
}

/* get the name of product
 * @product - the html that will be parsed
 * @returns - the name of product in a new allocated string*/
char *get_name(const char *product)
{
	char piv[] = "data-zone=\"title\">";
	char *start = strstr(product, piv);
	if(!start) {
		fprintf(stderr, "get_name: No start\n");
		return NULL;
	}
	start += strlen(piv);
	char *end = strstr(start, "</a>");
	if(!end) {
		fprintf(stderr, "get_name: No end\n");
		return NULL;
	}

	char *name = malloc(end-start + 1);
	strncpy(name, start, end-start);
	name[end-start] = '\0';
	return name;
}

// TODO: check the other price tag
float get_price(const char *product)
{
	char piv[] = "<p class=\"product-new-price\">";
	char *start = strstr(product, piv);
	if(!start) {
		fprintf(stderr, "get_price: No start\n");
		return -1;
	}
	start += strlen(piv);
	char nr[256] = {0};
	int i=0;
	// get the digits
	while(isdigit(start[i])) {
		nr[i++] = start[i];
	}
	nr[i] = '\0';
	// if this shit is here jump over
	if(start[i] == '&') {
		start += strlen("&#46;");
	}
	// and get the digits again
	while(isdigit(start[i])) {
		nr[i++] = start[i];
	}
	nr[i] = '\0';
	start = strchr(start, '>');
	if(!start) {
		fprintf(stderr, "get_price: Problem with '>'\n");
		return -1;
	}
	start++;
	nr[i++] = '.';
	while(isdigit(*start)){
		nr[i++] = *start;
		start++;
	}
	nr[i]='\0';
	// printf("get_price: nr: %s\n", nr);
	float price = atof(nr);
	return price;
}

/* get the id of product from url
 * @url - the url of product
 * @returns - a string with the id, has to be freed */
char *get_id(const char *url)
{
	char piv[] = "pd/";
	char *start = strstr(url, piv);
	if(!start) {
		fprintf(stderr, "get_id: No start\n");
		return NULL;
	}
	start += strlen(piv);
	char *stop = strchr(start, '/');
	if(!stop) {
		fprintf(stderr, "get_id: No stop\n");
		return NULL;
	}
	char *id = malloc(stop-start + 1);
	strncpy(id, start, stop-start);
	return id;
}

/* Get the number of pages
 * Emag web page was changed so this function will not work */
int get_no_page(const char* page)
{
	if(!page) {
		return -1;
	}
	vec ps = izolate_class(page, "span", "visible-xs visible-sm");
	if(!ps) {
		fprintf(stderr, "Cannot get the thing for page number\n");
		return -1;
	}
	printf("Primul: %s\n", (char*)ps->v[0]);
	vec_free(ps);
	return 0;
}

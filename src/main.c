#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

#include "vector.h"
#include "db.h"
#include "curl.h"
#include "html_parse.h"

#define err(x) do { fprintf(stderr, "File: %s, Line: %d\n", __FILE__, __LINE__);\
					perror(x);\
				}while(0);

/* map the file to proccess memory
 * @file_path - the name of the file to map
 * @n - a pointer to write the size of the file in*/
int map_file(const char *file_path, char **f_content, int *f_size)
{
	*f_size = -1;
	int fd = open(file_path, O_RDONLY);
	struct stat sb;

	if(fstat(fd, &sb) == -1) {
		err("fstat");
		return -1;
	}
	char* f_mem = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if(!f_mem) {
		err("mmap");
		return -1;
	}

	*f_content = f_mem;
	*f_size = sb.st_size;

	close(fd);

	return *f_size;
	
}

/* escape the thick, actually just cut the string before a tick (\`) */
int esc_tick(char *str)
{
	char *ugly = strchr(str, '\'');
	if(ugly) {
		*ugly=0;
	}
	return 0;
}

/* Do a simple test to make sure things are still working
 * maybe put this o a separate file for tests */
void test1()
{
	char *page = NULL;
	int p_size = 0;
	int res = map_file("files/index.html", &page, &p_size);
	if(res == -1) {
		err("map_file");
		exit(-1);
	}

	vec ps = izolate_class(page, "div", "container");
	if(!ps) {
		fprintf(stderr, "izolate returned NULL\n");
	}

	printf("ps length = %d\n", ps->length);

	vec_free(ps);

	munmap(page, p_size);
}

void test2()
{
	char *page = NULL;
	int p_size = 0;
	int res = map_file("files/test2.html", &page, &p_size);
	if(res == -1) {
		err("map_file");
		exit(-1);
	}

	vec ps = izolate_class(page, "div", "card-v2");
	if(!ps) {
		fprintf(stderr, "izolate returned NULL\n");
	}
	printf("ps len: %d\n", ps->length);
	for(int i=0; i<ps->length; ++i) {
		char *url = get_url(ps->v[i]);
		if(!url) {
			fprintf(stderr, "No url for ps[%d]\n", i);
			continue;
		}
		printf("URL: %s\n", url);
		char *id = get_id(url);
		if(!id) {
			fprintf(stderr, "No id found for ps[%d]\n", i);
		}
		esc_tick(id);
		printf("ID: %s\n", id);
		char *name = get_name(ps->v[i]);
		if(!name) {
			fprintf(stderr, "No name for ps[%d]\n", i);
		}
		esc_tick(name);
		printf("Name: %s\n", name);
		float price = get_price(ps->v[i]);
		printf("Price: %.2f\n", price);

		free(url);
		free(id);
		free(name);
	}

	printf("ps length = %d\n", ps->length);

	vec_free(ps);

	munmap(page, p_size);
}

/* get the url for page n
 * @url - the url of the category
 * @n - the page to go to
 * @return - a new string with the url to page n */
char* url_for_page(const char* url, int n)
{
	char *last_s = NULL;
	char *slash = strchr(url, '/');
	while(slash) {
		last_s = slash;
		slash = strchr(slash + 1, '/');
	}
	if(!last_s) {
		fprintf(stderr, "%s - %d: url_for_page: no '/'\n", __FILE__, __LINE__);
		return NULL;
	}

	// check if the page is already set
	char *p = last_s-1;
	while(isdigit(*p)) --p;
	if(strncmp(p-1, "/p", 2) == 0) {
		last_s = p-1;
	}

	char *new_url = malloc(last_s - url + 5);
	if(!new_url) {
		err("malloc");
		return NULL;
	}
	strncpy(new_url, url, last_s - url);
	snprintf(new_url + (last_s - url), last_s - url + 5, "/p%d/c", n);
	printf("new_url: %s\n", new_url);
	return new_url;
}

void usage(const char *prog)
{
	fprintf(stderr, "Usage: %s link\n", prog);
	fprintf(stderr, "The link must be to an emag category, not a psecific produs\n");
}

/* Parse the page and store the products in db
 * @page - string with the html to parse */
int parse_page(const char* page)
{
	// vec ps = izolate_class(page, "p", "a");
	vec ps = izolate_class(page, "div", "card-v2");
	if(!ps) {
		fprintf(stderr, "izolate returned NULL\n");
	}
	printf("ps len: %d\n", ps->length);
	for(int i=0; i<ps->length; ++i) {
		char *url = get_url(ps->v[i]);
		if(!url) {
			fprintf(stderr, "No url for ps[%d]\n", i);
			continue;
		}
		printf("URL: %s\n", url);
		char *id = get_id(url);
		if(!id) {
			fprintf(stderr, "No id found for ps[%d]\n", i);
		}
		esc_tick(id);
		printf("ID: %s\n", id);
		char *name = get_name(ps->v[i]);
		if(!name) {
			fprintf(stderr, "No name for ps[%d]\n", i);
		}
		esc_tick(name);
		printf("Name: %s\n", name);
		float price = get_price(ps->v[i]);
		printf("Price: %.2f\n", price);

		add_in_db(name, url, id, price);

		free(url);
		free(id);
		free(name);
	}

	vec_free(ps);
	return 0;
}

int main(int argc, char **argv)
{
	if(argc < 2) {
		usage(argv[0]);
		exit(1);
	}
	char *page = NULL;
	char *url = strdup(argv[1]);
	size_t p_size = 0;

	init_db();

	long res = 0;

	int next_page = 2;

	while((res = request(url, &page, &p_size)) == 200) {
		parse_page(page);
		char *new_url = url_for_page(url, next_page++);
		free(url);
		url = new_url;
		res = request(url, &page, &p_size);
		sleep(1);	// give some time to avoid capcha
	}

	printf("Closing the database\n");
	db_close();
	free(page);
	return 0;
}

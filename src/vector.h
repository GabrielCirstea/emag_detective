#ifndef H_VECTOR
#define H_VECTOR

/* check src/vector.c for the source code */

struct vector {
	int length;		// current length of array
	int size;		// max size
	int e_size;		// size of element
	void **v;
	void (*free_fn)(void* e);
};
typedef struct vector* vec;

vec vec_init(int length, int size, void (*free_fn)(void* e));
int vec_free(vec v);
int vec_push(vec v, void *e);

#endif

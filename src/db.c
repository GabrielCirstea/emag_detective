/* db.c */
#include <stdlib.h>
#include <stdio.h>
#include <sqlite3.h>
#include <time.h>
#include <string.h>

#define err(x) do { fprintf(stderr, "File: %s, Line: %d\n", __FILE__, __LINE__);\
					perror(x);\
				}while(0);

#define SQL_STR_SIZE 2048

/* HARDCODED
 * Just look for the tables */
static int the_tables(sqlite3 *db)
{
	char table1[] = "CREATE TABLE IF NOT EXISTS products "\
				   "(id TEXT primary key, name TEXT, url TEXT)";
	char table2[] = "CREATE TABLE IF NOT EXISTS prices "\
				   "(pk integer primary key, id TEXT, price REAL, date TEXT)";
	char *zErrMsg = NULL;

	int rc = sqlite3_exec(db, table1, NULL, 0, &zErrMsg);
	if(rc != SQLITE_OK) {
		fprintf(stderr, "Eror in table1: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}
	rc = sqlite3_exec(db, table2, NULL, 0, &zErrMsg);
	if(rc != SQLITE_OK) {
		fprintf(stderr, "Eror in table2: %s\n", zErrMsg);
		sqlite3_free(zErrMsg);
	}

	return 0;
}

static int b_s_cb(void *data, int argc, char **argv, char **azColName)
{
	for(int i=0; i<argc; ++i) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}

[[maybe_unused]] static int basic_select(sqlite3 *db)
{
	char sql[] = "SELECT name from products";
	int rc;
	char *errMsg = NULL;

	rc = sqlite3_exec(db, sql, b_s_cb, NULL, &errMsg);
	if(rc != SQLITE_OK){
		fprintf(stderr, "basic_select: Eror exec: %s\n", sql);
		return -1;
	}

	return 0;
}

static int count_callback(void *data, int argc, char **argv, char **azColName)
{
	if(argc == 0) {
		fprintf(stderr, "id count: argc is zero\n");
		return -1;
	}
	int *nr = data;
	*nr = atoi(argv[0]);
	return 0;
}

/* @db - the database
 * @id - the id of product
 * @returns - count(id) from table */
int db_count_id(sqlite3 *db, const char *id, const char *table)
{
	char sql[SQL_STR_SIZE];
   	sprintf(sql, "SELECT count(id) from %s where id=\"%s\"", table, id);
	int rc;
	char *errMsg = NULL;
	int nr = -1;
	rc = sqlite3_exec(db ,sql, count_callback, &nr, &errMsg);
	if(rc != SQLITE_OK){
		fprintf(stderr, "db_count_id: Eror exec: %s\n", sql);
		return -1;
	}
	return nr;
}

static int price_callback(void *data, int argc, char **argv, char **azColName)
{
	if(argc == 0) {
		fprintf(stderr, "price: argc is zero\n");
		return -1;
	}
	float *price = data;
	*price = atof(argv[0]);
	return 0;
}

/* Get the last price for a product
 * @db - the database
 * @id - the product id
 * @returns the last price, or -1 if fails */
float db_last_price(sqlite3 *db, const char *id)
{
	char sql[SQL_STR_SIZE];
   	sprintf(sql, "SELECT price from prices where id=\"%s\" "
			"ORDER BY date DESC LIMIT 1", id);
	int rc;
	char *errMsg = NULL;
	float price;
	rc = sqlite3_exec(db ,sql, price_callback, &price, &errMsg);
	if(rc != SQLITE_OK){
		fprintf(stderr, "db_last_price: Eror exec: %s\n", sql);
		return -1;
	}
	return price;
}

/* Insert a product in `products` table */
static int db_insert_product(sqlite3 *db, const char *name, const char *url, 
		const char *id)
{
	char sql[SQL_STR_SIZE];
	// dirty shit ahead
	char *ugly = strchr(name, '\'');
	if(ugly) {
		*ugly=0;
	}
   	sprintf(sql, "INSERT INTO products(id, name, url) VALUES ('%s', '%s', '%s')",
		   	id, name, url);
	int rc;
	char *errMsg = NULL;
	printf("insert: befor exec\n");
	rc = sqlite3_exec(db ,sql, NULL, NULL, &errMsg);
	printf("insert: after exec\n");
	if(rc != SQLITE_OK){
		fprintf(stderr, "db_insert_product: Eror exec: %s\n", sql);
		fprintf(stderr, "%s\n", errMsg);
		return -1;
	}
	return rc;
}

/* Insert the price for a product with the current time */
static int db_insert_price_now(sqlite3 *db, const char *id, float price)
{
	time_t t = time(NULL);
	struct tm time = *localtime(&t);
	printf("Year: %d, month: %d, day: %d\n", time.tm_year + 1900, time.tm_mon + 1,
			time.tm_mday);
	char d_time[256];
	sprintf(d_time, "%d-%d-%d", time.tm_mday, time.tm_mon + 1, time.tm_year + 1900);

	char sql[SQL_STR_SIZE];
   	sprintf(sql, "INSERT INTO prices (id, price, date) VALUES ('%s', %.2f, '%s')", id, price, d_time);
	int rc;
	char *errMsg = NULL;
	rc = sqlite3_exec(db ,sql, NULL, NULL, &errMsg);
	if(rc != SQLITE_OK){
		fprintf(stderr, "db_insert_price_now: Eror exec: %s\n", sql);
		fprintf(stderr, "%s\n", errMsg);
		return -1;
	}
	return rc;
}

static sqlite3* db_open()
{
	const char db_name[] = "product_prices.db";
	static sqlite3 *db = NULL;
	if (!db){
		printf("Open db\n");
		int rc = sqlite3_open(db_name, &db);
		if(rc) {
			fprintf(stderr, "Cannot connect to %s database: %s\n",
					db_name, sqlite3_errmsg(db));
			return NULL;
		}
	}
	return db;
}

int db_close()
{
	sqlite3 *db = db_open();
	sqlite3_close(db);
	return 0;
}

int init_db()
{
	sqlite3 *db = db_open();

	the_tables(db);
	return 0;
}

int add_in_db(const char *name, const char *url, const char *id, float price)
{
	sqlite3 *db = db_open();

	int count = db_count_id(db, id, "products");
	printf("The count: %d\n", count);
	if(!count) {
		printf("add_in_db: no count\n");
		db_insert_product(db, name, url, id);
		printf("add_in_db: after insert %d\n", __LINE__);
		db_insert_price_now(db, id, price);
	} else {
		printf("add_in_db: count\n");
		float n_price = db_last_price(db, id);
		if(price != n_price) {
			db_insert_price_now(db, id, price);
		}
	}
	float l_price = db_last_price(db, id);
	printf("The price: %.2f\n", l_price);

	return 0;
}

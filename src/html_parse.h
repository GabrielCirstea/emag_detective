#ifndef ED_HTML
#define ED_HTML

char *get_tag(const char *page);
char *strip_tag(const char *page);
vec izolate_class(const char *page, const char *tag, const char *class);
char *get_url(const char *product);
char *get_name(const char *product);
float get_price(const char *product);
char *get_id(const char *url);

#endif

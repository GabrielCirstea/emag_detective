# Emag detective

A script that pull, parse and store products name, and proces on a database for
later use.

## Just the beginning

The project just started and is not stable.
This project is a C implementation of another 
[project](https://github.com/ClaudiuMocanuVasile/badger-detective-server).

## TODO

* [] put the data in database
* [] get link from the CLI
* [] make a CLI in the first place
* [] dowload the html pages to parse
* [] check all the corner cases

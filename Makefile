
# still learning about make
CC=gcc
SRC := src
LIB := lib
OBJ := obj
DIRS=$(OBJ)
EXE=main
FLAGS=-l sqlite3 -l curl -g -O0 -Wall

SOURCES := $(wildcard $(SRC)/*.c)
OBJECTS := $(patsubst $(SRC)/%.c, $(OBJ)/%.o, $(SOURCES))

#all: $(EXE)
#	$(CC) -I$(SRC) $< -o $(OBJ)/$@

all: $(DIRS) $(OBJECTS) $(EXE)

# EXE
main: $(OBJ)/main.o $(OBJ)/html_parse.o $(SRC)/vector.h $(OBJ)/vector.o $(OBJ)/db.o
	$(CC) $^ -o $@  $(FLAGS)
# dirs
$(OBJ):
	mkdir $@

# objects
$(OBJ)/%.o: $(SRC)/%.c
	$(CC) -I$(SRC) $< -c -g -Wall -o $@

.PHONY: clean

clean:
	rm -f $(OBJ)/*.o $(EXE)
